CREATE DEFINER=`admin`@`%` PROCEDURE `prod_mcg_integration`.`replace_products`()
begin

    
-- *** A ***
    -- Declare general variables
    DECLARE total_affected_rows INT(11) DEFAULT NULL;
    DECLARE total_rows_datasource INT(11) DEFAULT NULL;
    DECLARE query_result TEXT;
    DECLARE etl_start_datetime DATETIME;
    DECLARE etl_status VARCHAR(15) DEFAULT 'failed';
    DECLARE is_new_records_inserted TINYINT(1) DEFAULT NULL;
    DECLARE total_old_records INT(10) DEFAULT '0';
    DECLARE total_new_records INT(10) DEFAULT '0';
    DECLARE etl_result TEXT DEFAULT NULL;
    DECLARE full_error TEXT DEFAULT NULL;
    
    -- Declare variables for storing error
    DECLARE error_sqlstate VARCHAR(5) DEFAULT '00000';
    DECLARE error_no VARCHAR(30);
    DECLARE error_text TEXT;
    
    DECLARE CONTINUE HANDLER FOR SQLEXCEPTION
    BEGIN
        GET DIAGNOSTICS CONDITION 1
        error_sqlstate = RETURNED_SQLSTATE, 
        error_no = MYSQL_ERRNO, 
        error_text = MESSAGE_TEXT;
    END;
    
    -- Start executing query
    SET etl_start_datetime = NOW(); 
    
    -- Count total row from original source
    SELECT 
        COUNT(*) INTO total_rows_datasource 
    FROM prod_mcg_staging.sap_article;
-- *** A ***
-- *** B ***
    -- Count total row before inserting new records
    SELECT
        COUNT(*) INTO total_old_records
    FROM prod_mcg_integration.products;

-- RUN --
    
TRUNCATE prod_mcg_integration.products;

INSERT INTO prod_mcg_integration.products
SELECT
    material PRODUCT_ID,
    IF (LENGTH(material) = 13, SUBSTRING(material, 1, 9), NULL) PRODUCT_CORE_ID,
    article_description NAME,
    IF (x_site_status = '01', 0, 1) IS_ACTIVE,
    NULL IS_FOCUSED,
    fashion_grade_desc TYPE,
    NULL ATTRIBUTE_GROUP_ID,
    standard_cost_adj COST,
    selling_price PRICE,
    mcl1text CATEGORY_LEVEL_1,
    mcl2text CATEGORY_LEVEL_2,
    mcl3text CATEGORY_LEVEL_3,
    mcl4text CATEGORY_LEVEL_4,
    mcl5text CATEGORY_LEVEL_5,
    concat(season_year, IF (season = '0000', '', SUBSTRING(season, 3, 2))) AS START_DATE,
    NULL END_DATE,
    NULL FIRST_BUY_DATE,
    NULL LAST_BUY_DATE,
    gender GENDER,
    product_line_text PRODUCT_LINE,
    sub_brand_text SUB_BRAND,
    article_color COLOR,
    article_size SIZE,
    shape_1_text SHAPE_1,
    shape_2_text SHAPE_2,
    shape_3_text SHAPE_3,
    IF (prd_attr_x = 'D', 1, 0) AS IS_DENIM,
    aging_color,
    aging_color_text
FROM
    prod_mcg_staging.sap_article;


-- UPDATE is_focused
update prod_mcg_integration.products
SET
    IS_FOCUSED = 1
WHERE
    CATEGORY_LEVEL_2 NOT IN ('', 'ANELLO', 'BEAUTY BUFFET', 'MC Space', 'NATURETUCH', 'OTHER', 'OTHER BRANDS', 'RAYBAN');


-- Special cases for a report but not for models
/*
update prod_mcg_integration.products
SET
    IS_FOCUSED = 9
WHERE
    PRODUCT_ID IN ('000000006000000001', '000000006000000002');
*/


-- RUN --

    -- Count total row after inserting new records
    SELECT
        COUNT(*) INTO total_new_records
    FROM prod_mcg_integration.products;
    
-- *** B ***
-- *** C ***

    IF total_new_records - total_old_records > 0 THEN
        SET is_new_records_inserted = 0;
    ELSE
        SET is_new_records_inserted = 1;
    END IF;

    IF total_new_records - total_old_records > 0 THEN
        SET is_new_records_inserted = 1;
    ELSE
        SET is_new_records_inserted = 0;
    END IF;

    
    -- Check whether the insert was successful
    IF error_sqlstate = '00000' THEN
        GET DIAGNOSTICS total_affected_rows = ROW_COUNT;
        SET etl_result = CONCAT('Insert succeeded, row count = ', total_affected_rows);
        SET etl_status = 'succeeded';
    ELSE
        SET etl_result = CONCAT("ERROR ", error_sqlstate, " (", error_no, "): ", error_text);
        SET full_error = CONCAT("ERROR ", error_sqlstate, " (", error_no, "): ", error_text);
    END IF;
  
    -- insert etl log
    CALL prod_mcg_integration.insert_etl_log(
        etl_start_datetime, -- param_etl_start_timestamp
        NOW(), -- param_etl_finish_timestamp
        etl_status, -- param_etl_status
        'Stored Procedure', -- param_etl_done_by
        'prod_mcg_integration.replace_products()',-- param_script_name
        '-', -- param_type_of_data
        '-', -- param_source_system_name
        NULL, -- param_source_path
        'prod_mcg_staging', -- param_source_schema_name
        'sap_article',
        '-', -- param_target_system_name
        NULL, -- param_target_path
        'prod_mcg_integration', -- param_target_schema_name
        'products',
        total_new_records - total_old_records, -- param_total_new_rows
        total_new_records, -- param_total_rows_in_target_system
        is_new_records_inserted, -- param_is_new_records_inserted
        'replace_products',
        full_error, -- param_error_log
        NULL -- param_remark
    );

-- *** C ***


end;
