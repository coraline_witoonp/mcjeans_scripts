CREATE DEFINER=`admin`@`%` PROCEDURE `prod_mcg_integration`.`insert_etl_log`(
    IN param_etl_start_timestamp DATETIME,
    IN param_etl_finish_timestamp DATETIME,
    IN param_etl_status VARCHAR(10),
    IN param_etl_done_by VARCHAR(100),
    IN param_script_name VARCHAR(255),
    IN param_type_of_data VARCHAR(100),
    IN param_source_system_name VARCHAR(100),
    IN param_source_path VARCHAR(255),
    IN param_source_schema_name VARCHAR(100),
    IN param_datasource_name VARCHAR(100),
    IN param_target_system_name VARCHAR(100),
    IN param_target_path VARCHAR(255),
    IN param_target_schema_name VARCHAR(100),
    IN param_target_name VARCHAR(100),
    IN param_total_new_rows INT(10),
    IN param_total_rows_after_update INT(10),
    IN param_is_new_records_inserted TINYINT(1),
    IN param_etl_description TEXT,
    IN param_error_log TEXT,
    IN param_remark TEXT
)
BEGIN
    
    INSERT INTO prod_mcg_integration.etl_log
    (
        etl_start_datetime,
        etl_finish_datetime,
        etl_status,
        etl_done_by,
        script_name,
        type_of_data,
        source_system_name,
        source_path,
        source_schema_name,
        datasource_name,
        target_system_name,
        target_path,
        target_schema_name,
        target_name,
        total_new_rows,
        total_rows_after_update,
        is_new_records_inserted,
        etl_description,
        error_log,
        remark
    )
    VALUES
    (
        param_etl_start_timestamp,
        param_etl_finish_timestamp,
        param_etl_status,
        param_etl_done_by,
        param_script_name,
        param_type_of_data,
        param_source_system_name,
        param_source_path,
        param_source_schema_name,
        param_datasource_name,
        param_target_system_name,
        param_target_path,
        param_target_schema_name,
        param_target_name,
        param_total_new_rows,
        param_total_rows_after_update,
        param_is_new_records_inserted,
        param_etl_description,
        param_error_log,
        param_remark
    );

END;
