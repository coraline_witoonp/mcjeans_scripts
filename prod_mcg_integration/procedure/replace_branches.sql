CREATE DEFINER=`admin`@`%` PROCEDURE `prod_mcg_integration`.`replace_branches`()
begin
    
    
-- *** A ***
    -- Declare general variables
    DECLARE total_affected_rows INT(11) DEFAULT NULL;
    DECLARE total_rows_datasource INT(11) DEFAULT NULL;
    DECLARE query_result TEXT;
    DECLARE etl_start_datetime DATETIME;
    DECLARE etl_status VARCHAR(15) DEFAULT 'failed';
    DECLARE is_new_records_inserted TINYINT(1) DEFAULT NULL;
    DECLARE total_old_records INT(10) DEFAULT '0';
    DECLARE total_new_records INT(10) DEFAULT '0';
    DECLARE etl_result TEXT DEFAULT NULL;
    DECLARE full_error TEXT DEFAULT NULL;
    
    -- Declare variables for storing error
    DECLARE error_sqlstate VARCHAR(5) DEFAULT '00000';
    DECLARE error_no VARCHAR(30);
    DECLARE error_text TEXT;
    
    DECLARE CONTINUE HANDLER FOR SQLEXCEPTION
    BEGIN
        GET DIAGNOSTICS CONDITION 1
        error_sqlstate = RETURNED_SQLSTATE, 
        error_no = MYSQL_ERRNO, 
        error_text = MESSAGE_TEXT;
    END;
    
    -- Start executing query
    SET etl_start_datetime = NOW(); 
    
    -- Count total row from original source
    SELECT 
        COUNT(*) INTO total_rows_datasource 
    FROM prod_mcg_staging.sap_site;
-- *** A ***
-- *** B ***
    -- Count total row before inserting new records
    SELECT
        COUNT(*) INTO total_old_records
    FROM prod_mcg_integration.branches ;


-- run --
    
    truncate prod_mcg_integration.branches;

    insert into prod_mcg_integration.branches (
    branch_id,
    name,
    search_term,
    is_focused,
    `type`,
    attribute_group_id,
    region,
    province,
    postal_code,
    latitude,
    longitude,
    address,
    `size`,
    storage_capacity,
    storefront_capacity,
    business_type,
    shop_group,
    start_date,
    end_date,
    first_buy_date,
    last_buy_date
)
select
    sap.customer as branch_id,
    sap.name as name,
    sap.search_term_1 as search_term,
        case when (sap.name_3 like '%ปิด%') 
            then 9 
         when (sap.customer in ('d701','d703','d704','d705','d708','d709','d710','d711',
                                'd335','d336','d337','d338','d339','d340',
                                'd028','d029','d030','d034','d039','d040','d066','d069','d089',
                                'd105','d127','d129','d130','d131','d133','d134','d135','d136',
                                'd137','d138','d139','d142','d143','d146','d147','d148','d149',
                                'd150','d152','d153','d154','d155','d156','d157','d158','d160',
                                'd162','d164','d165','d167','d169','d171','d172','d173','d175',
                                'd177','d183','d186','d187','d188','d189','d190','d191','d192',
                                'd193','d194','d195','d196','d197','d199',
                                'd200','d201','d202','d203','d204','d205','d206','d207','d208',
                                'd209','d210','d211','d212','d213','d214','d215','d216','d217',
                                'd218','d219','d220','d221','d222','d223','d224','d225','d226',
                                'd227','d228','d229','d230','d231','d232','d233','d234','d235',
                                'd236','d237','d238','d239','d240','d241','d242','d243','d244',
                                'd245','d246','d247','d248','d249','d250','d257','d258','d262',
                                'd263','d265','d266','d267','d269','d275','d277','d278','d294',
                                'd295','d296','d299',
                                'd305','d306','d307','d308','d309','d310','d311','d312','d313',
                                'd314','d315','d316','d317','d318','d327','d328','d329','d330','d345',
                                'p001','p007',
                                's065','s089','s111','s119','s121','s123','s124','s126','s128',
                                's129','s130','s132','s133','s138','s140','s141','s146','s149',
                                's165','s168','s177','s183','s184','s186','s190','s202','s203',
                                's209','s215','s217','s219','s221','s228','s229','s232','s233',
                                's234','s235','s238','s242','s244','s251','s252','s275','s278',
                                's304','s324',
                                'x038','x099','x182','x183',
                                'd047','d179','d181','d182','d184','d185','d198','d259','d260',
                                'd261','d264','d271','d335','d336','d337','d338','d339','d340',
                                'd344','d352','d700','d701','d705','d708',
                                'p009','p019',
                                's258',
                                'd712',
                                's700') 
             and sap.is_focus = 1) then 8 
        else sap.is_focus end as is_focused,
    
    sap.site_profile_name as `type`,
    null as attribute_group_id,
    sap.custgrp3_desc as region,
    sap.region_province as province,
    sap.postal_code as postal_code,
    null as latitude,
    null as longitude,
    
    concat(replace(sap.street, 'xxxxxxxxxxxxxxxxxxxx', ''),
           replace(sap.street_4, 'xxxxxxxxxxxxxxxxxxxx', ''),
           replace(sap.street_5, 'xxxxxxxxxxxxxxxxxxxx', ''),
           replace(sap.district, 'xxxxxxxxxxxxxxxxxxxx', ''),
           replace(sap.region_province, 'xxxxxxxxxxxxxxxxxxxx', '')) as address,
           
    null as `size`,
    null as storage_capacity,
    null as storefront_capacity,
    sap.distr_chl_desc as business_type,
    sap.sub_group_text as shop_group,
    opening_date as start_date,
    closing_date as end_date,
    null as first_buy_date, --  == 
    null as last_buy_date -- ===
from
    prod_mcg_staging.sap_site as sap;
-- ====
update prod_mcg_integration.branches as brn
left join prod_mcg_staging.shop_location as shl on
    brn.branch_id = shl.`store code`
set
    brn.latitude = shl.latitude,
    brn.longitude = shl.longitude;
-- ====
update prod_mcg_integration.branches as brn
left join prod_mcg_staging.shop_capacity as shc on
    brn.branch_id = shc.`branch_id`
set
    brn.storage_capacity = shc.storage_capacity,
    brn.storefront_capacity = shc.storefront_capacity;
    
-- ====

update prod_mcg_integration.branches as brn
left join prod_mcg_staging.shop_area as sha on
    brn.branch_id = sha.`branch_id`
set
    brn.`size` = sha.area;

-- ====

update prod_mcg_integration.branches as brn
left join prod_mcg_staging.logistic_cost_and_leadtime as lcl on
    brn.province = lcl.province
set
    brn.leadtime = lcl.leadtime_days ;
-- ====

/*update prod_mcg_integration.branches as brn
left join prod_mcg_staging.branchs_cost as brc on
    brn.branch_id = brc.customer
set
    -- brn.region  = brc.region ,
    brn.storefront_capacity = brc.shop,
    brn.storage_capacity = brc.stock ,
    brn.labor_cost = brc.labor_cost,
    brn.insurance_fee = brc.insurance_fee ,
    brn.utility_bills = brc.utility_bills;*/
-- ===
update prod_mcg_integration.branches as brn
left join prod_mcg_staging.branchs_cost as brc on
    brn.branch_id = brc.customer
set
    -- brn.region  = brc.region ,
    brn.storefront_capacity = brc.shop,
    brn.storage_capacity = brc.stock ,
    brn.labor_cost = brc.labor_cost,
    brn.insurance_fee = brc.insurance_fee ,
    brn.utility_bills = brc.utility_bills,
    brn.rental_fee = brc.rental_fee,
    brn.delivery_cost = brc.delivery_cost;

-- run --

    -- Count total row after inserting new records
    SELECT
        COUNT(*) INTO total_new_records
    FROM prod_mcg_integration.branches ;
    
-- *** B ***
-- *** C ***

    IF total_new_records - total_old_records > 0 THEN
        SET is_new_records_inserted = 0;
    ELSE
        SET is_new_records_inserted = 1;
    END IF;

    IF total_new_records - total_old_records > 0 THEN
        SET is_new_records_inserted = 1;
    ELSE
        SET is_new_records_inserted = 0;
    END IF;

    
    -- Check whether the insert was successful
    IF error_sqlstate = '00000' THEN
        GET DIAGNOSTICS total_affected_rows = ROW_COUNT;
        SET etl_result = CONCAT('Insert succeeded, row count = ', total_affected_rows);
        SET etl_status = 'succeeded';
    ELSE
        SET etl_result = CONCAT("ERROR ", error_sqlstate, " (", error_no, "): ", error_text);
        SET full_error = CONCAT("ERROR ", error_sqlstate, " (", error_no, "): ", error_text);
    END IF;
  
    -- insert etl log
    CALL prod_mcg_integration.insert_etl_log(
        etl_start_datetime, -- param_etl_start_timestamp
        NOW(), -- param_etl_finish_timestamp
        etl_status, -- param_etl_status
        'Stored Procedure', -- param_etl_done_by
        'prod_mcg_integration.replace_branches()',-- param_script_name
        '-', -- param_type_of_data
        '-', -- param_source_system_name
        NULL, -- param_source_path
        'prod_mcg_staging', -- param_source_schema_name
        'sap_site',
        '-', -- param_target_system_name
        NULL, -- param_target_path
        'prod_mcg_integration', -- param_target_schema_name
        'branches',
        total_new_records - total_old_records, -- param_total_new_rows
        total_new_records, -- param_total_rows_in_target_system
        is_new_records_inserted, -- param_is_new_records_inserted
        'replace_branches',
        full_error, -- param_error_log
        NULL -- param_remark
    );

-- *** C ***

    
END;
