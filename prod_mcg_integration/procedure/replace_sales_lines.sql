CREATE DEFINER=`admin`@`%` PROCEDURE `prod_mcg_integration`.`replace_sales_lines`()
BEGIN


    
-- *** A ***
    -- Declare general variables
    DECLARE total_affected_rows INT(11) DEFAULT NULL;
    DECLARE total_rows_datasource INT(11) DEFAULT NULL;
    DECLARE query_result TEXT;
    DECLARE etl_start_datetime DATETIME;
    DECLARE etl_status VARCHAR(15) DEFAULT 'failed';
    DECLARE is_new_records_inserted TINYINT(1) DEFAULT NULL;
    DECLARE total_old_records INT(10) DEFAULT '0';
    DECLARE total_new_records INT(10) DEFAULT '0';
    DECLARE etl_result TEXT DEFAULT NULL;
    DECLARE full_error TEXT DEFAULT NULL;
    
    -- Declare variables for storing error
    DECLARE error_sqlstate VARCHAR(5) DEFAULT '00000';
    DECLARE error_no VARCHAR(30);
    DECLARE error_text TEXT;
    
    DECLARE CONTINUE HANDLER FOR SQLEXCEPTION
    BEGIN
        GET DIAGNOSTICS CONDITION 1
        error_sqlstate = RETURNED_SQLSTATE, 
        error_no = MYSQL_ERRNO, 
        error_text = MESSAGE_TEXT;
    END;
    
    -- Start executing query
    SET etl_start_datetime = NOW(); 
    
    -- Count total row from original source
    SELECT 
        COUNT(*) INTO total_rows_datasource 
    FROM prod_mcg_staging.sale_line;
-- *** A ***
-- *** B ***
    -- Count total row before inserting new records
    SELECT
        COUNT(*) INTO total_old_records
    FROM prod_mcg_integration.sales_lines;

-- RUN --
    
replace INTO prod_mcg_integration.sales_lines (
    sales_head_id,
    void_ref_id,
    `sequence`,
    `type`,
    sold_date,
    customer_id,
    branch_id,
    sales_channel,
    branch_ref,
    product_id,
    product_name,
    qty,
    sales_amount,
    discount_amount,
    coupon_amount,
    promotion_id,
    is_free,
    with_promotion
)
SELECT
    code AS sales_head_id,
    null as void_ref_id,
    coalesce(seq,0) AS `sequence`,
    '' AS `type`,
    sold_date AS sold_date,
    if (customer_code like '0%' or customer_code = "" , null, customer_code ) AS customer_id,
    branch_code AS branch_id,
    '' as sales_channel,
    NULL AS branch_ref,
    item_code AS product_id,
    item_name AS product_name,
    -- price AS price,
    quantity AS qty,
    -- vat_rate AS vat,
    inc_vat_price + IFNULL(coupon_incl_vat,0) sales_amount,
    IFNULL(discount_amount,0) discount_amount,
    IFNULL(coupon_incl_vat,0) coupon_amount,
    left(promotion_code,30) AS promotion_id,
    free_flag AS is_free,
    (case when promotion_code in ('PRO-NORMALJ','PRO-NORMAL','PRO-PNTONTP','') then 0 else 1 end) AS with_promotion
FROM
    prod_mcg_staging.sale_line
where
    sold_date = DATE_ADD(CURDATE(), INTERVAL -1 day);






/*
update prod_mcg_integration.sales_lines 
set with_promotion = if(promotion_id in ('PRO-NORMALJ','PRO-NORMAL','PRO-PNTONTP','') , 0,1)*/
--  ==
update prod_mcg_integration.sales_heads as sah
    join prod_mcg_integration.sales_lines as sal on
    sah.sales_head_id = sal.sales_head_id
set
    sal.`TYPE` =  sah.`TYPE` ,
    sal.VOID_REF_ID = sah.VOID_REF_ID, 
    sal.SALES_CHANNEL = sah.SALES_CHANNEL, 
    sal.BRANCH_REF = sah.BRANCH_REF
where
    sah.sold_date = DATE_ADD(CURDATE(), INTERVAL -1 day);

--  ===
-- === 
DROP TEMPORARY TABLE if exists prod_mcg_integration.temp_table_total_qty;
CREATE TEMPORARY TABLE prod_mcg_integration.temp_table_total_qty
(INDEX temp_index_name (sales_head_id))
select
    sales_head_id,
    sum(qty) as sales_head_total_qty
from
    prod_mcg_integration.sales_lines
where
    sold_date = DATE_ADD(CURDATE(), INTERVAL -1 day)
group by
    sales_head_id;

update prod_mcg_integration.sales_heads as sah
    join prod_mcg_integration.temp_table_total_qty as sal on
    sah.sales_head_id = sal.sales_head_id
set
    sah.total_qty =  sal.sales_head_total_qty;

DROP TEMPORARY TABLE if exists prod_mcg_integration.temp_table_total_qty;
-- ===
-- ***
DROP TEMPORARY TABLE if exists prod_mcg_integration.temp_table_branch_id;

CREATE TEMPORARY TABLE prod_mcg_integration.temp_table_branch_id
(INDEX temp_index_name (branch_id))
select
    branch_id,
    min(sold_date) FIRST_BUY_DATE,
    max(sold_date) LAST_BUY_DATE
from
    prod_mcg_integration.sales_lines
group by
    branch_id;

update prod_mcg_integration.branches as b
inner join prod_mcg_integration.temp_table_branch_id as txn on 
    b.branch_id = txn.branch_id
set 
    b.FIRST_BUY_DATE = txn.FIRST_BUY_DATE;

update prod_mcg_integration.branches as b
inner join prod_mcg_integration.temp_table_branch_id as txn on 
    b.branch_id = txn.branch_id
set b.LAST_BUY_DATE = txn.LAST_BUY_DATE;

DROP TEMPORARY TABLE  if exists prod_mcg_integration.temp_table_branch_id; 
-- ***

-- *** เอาไปใส่ใน Sale line
-- Update FIRST_BUY_DATE, LAST_BUY_DATE

DROP TEMPORARY TABLE  if exists prod_mcg_integration.temp_table_product_id;
CREATE TEMPORARY TABLE prod_mcg_integration.temp_table_product_id
(INDEX temp_index_name (PRODUCT_ID))
select
    PRODUCT_ID,
    min(sold_date) FIRST_BUY_DATE,
    max(sold_date) LAST_BUY_DATE
from
    prod_mcg_integration.sales_lines
group by
    PRODUCT_ID;

update prod_mcg_integration.products as p
inner join prod_mcg_integration.temp_table_product_id as txn on 
    p.PRODUCT_ID = txn.PRODUCT_ID
set p.FIRST_BUY_DATE = txn.FIRST_BUY_DATE;

update prod_mcg_integration.products as p
inner join prod_mcg_integration.temp_table_product_id as txn on 
    p.PRODUCT_ID = txn.PRODUCT_ID
set p.LAST_BUY_DATE = txn.LAST_BUY_DATE;

DROP TEMPORARY TABLE  if exists prod_mcg_integration.temp_table_product_id;
-- where sold_date >= วันที่ข้อมูล sale_line ใหม่ที่เกิดการขาย

-- RUN --

    -- Count total row after inserting new records
    SELECT
        COUNT(*) INTO total_new_records
    FROM prod_mcg_integration.sales_lines;
    
-- *** B ***
-- *** C ***

    IF total_new_records - total_old_records > 0 THEN
        SET is_new_records_inserted = 0;
    ELSE
        SET is_new_records_inserted = 1;
    END IF;

    IF total_new_records - total_old_records > 0 THEN
        SET is_new_records_inserted = 1;
    ELSE
        SET is_new_records_inserted = 0;
    END IF;

    
    -- Check whether the insert was successful
    IF error_sqlstate = '00000' THEN
        GET DIAGNOSTICS total_affected_rows = ROW_COUNT;
        SET etl_result = CONCAT('Insert succeeded, row count = ', total_affected_rows);
        SET etl_status = 'succeeded';
    ELSE
        SET etl_result = CONCAT("ERROR ", error_sqlstate, " (", error_no, "): ", error_text);
        SET full_error = CONCAT("ERROR ", error_sqlstate, " (", error_no, "): ", error_text);
    END IF;
  
    -- insert etl log
    CALL prod_mcg_integration.insert_etl_log(
        etl_start_datetime, -- param_etl_start_timestamp
        NOW(), -- param_etl_finish_timestamp
        etl_status, -- param_etl_status
        'Stored Procedure', -- param_etl_done_by
        'prod_mcg_integration.replace_sales_lines()',-- param_script_name
        '-', -- param_type_of_data
        '-', -- param_source_system_name
        NULL, -- param_source_path
        'prod_mcg_staging', -- param_source_schema_name
        'sales_lines',
        '-', -- param_target_system_name
        NULL, -- param_target_path
        'prod_mcg_integration', -- param_target_schema_name
        'sales_lines',
        total_new_records - total_old_records, -- param_total_new_rows
        total_new_records, -- param_total_rows_in_target_system
        is_new_records_inserted, -- param_is_new_records_inserted
        'replace_sales_lines',
        full_error, -- param_error_log
        NULL -- param_remark
    );

-- *** C ***

END;
