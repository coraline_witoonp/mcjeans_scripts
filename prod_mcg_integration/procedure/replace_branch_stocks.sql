CREATE DEFINER=`admin`@`%` PROCEDURE `prod_mcg_integration`.`replace_branch_stocks`()
begin
    
    
-- *** A ***
    -- Declare general variables
    DECLARE total_affected_rows INT(11) DEFAULT NULL;
    DECLARE total_rows_datasource INT(11) DEFAULT NULL;
    DECLARE query_result TEXT;
    DECLARE etl_start_datetime DATETIME;
    DECLARE etl_status VARCHAR(15) DEFAULT 'failed';
    DECLARE is_new_records_inserted TINYINT(1) DEFAULT NULL;
    DECLARE total_old_records INT(10) DEFAULT '0';
    DECLARE total_new_records INT(10) DEFAULT '0';
    DECLARE etl_result TEXT DEFAULT NULL;
    DECLARE full_error TEXT DEFAULT NULL;
    
    -- Declare variables for storing error
    DECLARE error_sqlstate VARCHAR(5) DEFAULT '00000';
    DECLARE error_no VARCHAR(30);
    DECLARE error_text TEXT;
    
    DECLARE CONTINUE HANDLER FOR SQLEXCEPTION
    BEGIN
        GET DIAGNOSTICS CONDITION 1
        error_sqlstate = RETURNED_SQLSTATE, 
        error_no = MYSQL_ERRNO, 
        error_text = MESSAGE_TEXT;
    END;
    
    -- Start executing query
    SET etl_start_datetime = NOW(); 
    
    -- Count total row from original source
    SELECT 
        COUNT(*) INTO total_rows_datasource 
    FROM prod_mcg_staging.sap_zmmr021;
-- *** A ***
-- *** B ***
    -- Count total row before inserting new records
    SELECT
        COUNT(*) INTO total_old_records
    FROM prod_mcg_integration.branch_stock;

-- RUN --
    
    
TRUNCATE prod_mcg_integration.branch_stock ;
-- Warehouse
INSERT IGNORE INTO prod_mcg_integration.branch_stock 
SELECT
    site as branch_id,
    article as product_id_sku,
    NULL  as product_id_model,
    NULL  as lot_no,
    sum(CASE WHEN sloc = 2001 THEN available ELSE 0 END)  as qty,
    now()  as current_datetime,
    sum(CASE WHEN sloc = '' THEN in_transit ELSE 0 END)  as in_transit_qty,
    NULL  as in_transit_datetime,
    sum(CASE WHEN sloc = '' THEN in_transit ELSE 0 END)  as on_delivery,
    0 as on_order,
    now()  as latest_etl_datetime
FROM
    prod_mcg_staging.sap_zmmr021
WHERE
    site = 1101
GROUP BY
    article,
    site;
-- Department
INSERT IGNORE INTO prod_mcg_integration.branch_stock 
SELECT
    site branch_id,
    article product_id_sku,
    NULL product_id_model,
    NULL lot_no,
    sum(CASE WHEN sloc IN (2001, 2002) THEN available ELSE 0 END) qty,
    now() current_datetime,
    sum(CASE WHEN sloc = 7001 THEN bun WHEN sloc IN (2001, 2002) THEN on_order ELSE 0 END) in_transit_qty,
    NULL in_transit_datetime,
    sum(CASE WHEN sloc = 7001 THEN bun ELSE 0 END)  as on_delivery,
    sum(CASE WHEN sloc IN (2001, 2002) THEN on_order ELSE 0 END)  as on_order,
    now() latest_etl_datetime
FROM
    prod_mcg_staging.sap_zmmr021
WHERE
    LEFT(site, 1) = 'D'
GROUP BY
    article,
    site;
-- Shop
INSERT IGNORE INTO prod_mcg_integration.branch_stock 
SELECT
    site branch_id,
    article product_id_sku,
    NULL product_id_model,
    NULL lot_no,
    sum(CASE WHEN sloc IN (2001, 2002) THEN available ELSE 0 END) qty,
    now() current_datetime,
    sum(CASE WHEN sloc = '' THEN in_transit WHEN sloc IN (2001, 2002) THEN on_order ELSE 0 END) in_transit_qty,
    NULL in_transit_datetime,
    sum(CASE WHEN sloc = '' THEN in_transit ELSE 0 END)  as on_delivery,
    sum(CASE WHEN sloc IN (2001, 2002) THEN on_order ELSE 0 END)  as on_order,
    now() latest_etl_datetime
FROM
    prod_mcg_staging.sap_zmmr021
WHERE
    site <> 1101 AND LEFT (site, 1) <> 'D'
GROUP BY
    article,
    site;


-- RUN --

    -- Count total row after inserting new records
    SELECT
        COUNT(*) INTO total_new_records
    FROM prod_mcg_integration.branch_stock;
    
-- *** B ***
-- *** C ***

    IF total_new_records - total_old_records > 0 THEN
        SET is_new_records_inserted = 0;
    ELSE
        SET is_new_records_inserted = 1;
    END IF;

    IF total_new_records - total_old_records > 0 THEN
        SET is_new_records_inserted = 1;
    ELSE
        SET is_new_records_inserted = 0;
    END IF;

    
    -- Check whether the insert was successful
    IF error_sqlstate = '00000' THEN
        GET DIAGNOSTICS total_affected_rows = ROW_COUNT;
        SET etl_result = CONCAT('Insert succeeded, row count = ', total_affected_rows);
        SET etl_status = 'succeeded';
    ELSE
        SET etl_result = CONCAT("ERROR ", error_sqlstate, " (", error_no, "): ", error_text);
        SET full_error = CONCAT("ERROR ", error_sqlstate, " (", error_no, "): ", error_text);
    END IF;
  
    -- insert etl log
    CALL prod_mcg_integration.insert_etl_log(
        etl_start_datetime, -- param_etl_start_timestamp
        NOW(), -- param_etl_finish_timestamp
        etl_status, -- param_etl_status
        'Stored Procedure', -- param_etl_done_by
        'prod_mcg_integration.replace_branch_stocks()',-- param_script_name
        '-', -- param_type_of_data
        '-', -- param_source_system_name
        NULL, -- param_source_path
        'prod_mcg_staging', -- param_source_schema_name
        'sap_zmmr021',
        '-', -- param_target_system_name
        NULL, -- param_target_path
        'prod_mcg_integration', -- param_target_schema_name
        'branch_stocks',
        total_new_records - total_old_records, -- param_total_new_rows
        total_new_records, -- param_total_rows_in_target_system
        is_new_records_inserted, -- param_is_new_records_inserted
        'replace_branch_stocks',
        full_error, -- param_error_log
        NULL -- param_remark
    );

-- *** C ***

END;
