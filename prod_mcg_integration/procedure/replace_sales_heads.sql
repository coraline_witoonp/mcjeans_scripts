CREATE DEFINER=`admin`@`%` PROCEDURE `prod_mcg_integration`.`replace_sales_heads`()
begin 

    
-- *** A ***
    -- Declare general variables
    DECLARE total_affected_rows INT(11) DEFAULT NULL;
    DECLARE total_rows_datasource INT(11) DEFAULT NULL;
    DECLARE query_result TEXT;
    DECLARE etl_start_datetime DATETIME;
    DECLARE etl_status VARCHAR(15) DEFAULT 'failed';
    DECLARE is_new_records_inserted TINYINT(1) DEFAULT NULL;
    DECLARE total_old_records INT(10) DEFAULT '0';
    DECLARE total_new_records INT(10) DEFAULT '0';
    DECLARE etl_result TEXT DEFAULT NULL;
    DECLARE full_error TEXT DEFAULT NULL;
    
    -- Declare variables for storing error
    DECLARE error_sqlstate VARCHAR(5) DEFAULT '00000';
    DECLARE error_no VARCHAR(30);
    DECLARE error_text TEXT;
    
    DECLARE CONTINUE HANDLER FOR SQLEXCEPTION
    BEGIN
        GET DIAGNOSTICS CONDITION 1
        error_sqlstate = RETURNED_SQLSTATE, 
        error_no = MYSQL_ERRNO, 
        error_text = MESSAGE_TEXT;
    END;
    
    -- Start executing query
    SET etl_start_datetime = NOW(); 
    
    -- Count total row from original source
    SELECT 
        COUNT(*) INTO total_rows_datasource 
    FROM prod_mcg_staging.sale_head;
-- *** A ***
-- *** B ***
    -- Count total row before inserting new records
    SELECT
        COUNT(*) INTO total_old_records
    FROM prod_mcg_integration.sales_heads;

-- RUN --
    
    
replace into prod_mcg_integration.sales_heads(
    sales_head_id,
    void_ref_id,
    `type`,
    sold_date,
    invoice_no,
    total_qty,
    total_amount,
    total_discount,
    customer_id,
    branch_id,
    branch_ref,
    sales_channel
)
SELECT
    code as sales_head_id,
    nullif (void_ref_code, "") as void_ref_id,
    nullif (`type`, "") as `type`,
    sold_date,
    sap_id as invoice_no,
    0 as total_qty,
    sum_inc_vat as total_amount,
    sum_discount as total_discount,
    nullif(customer_code, "") as customer_id,
    branch_code as branch_id,
    nullif(branch_ref, "") as branch_ref,
    nullif(sales_channel, "") as sales_channel
FROM
    prod_mcg_staging.sale_head
where
    sold_date >= DATE_ADD(CURDATE(), INTERVAL -1 day);


-- RUN --

    -- Count total row after inserting new records
    SELECT
        COUNT(*) INTO total_new_records
    FROM prod_mcg_integration.sales_heads;
    
-- *** B ***
-- *** C ***

    IF total_new_records - total_old_records > 0 THEN
        SET is_new_records_inserted = 0;
    ELSE
        SET is_new_records_inserted = 1;
    END IF;

    IF total_new_records - total_old_records > 0 THEN
        SET is_new_records_inserted = 1;
    ELSE
        SET is_new_records_inserted = 0;
    END IF;

    
    -- Check whether the insert was successful
    IF error_sqlstate = '00000' THEN
        GET DIAGNOSTICS total_affected_rows = ROW_COUNT;
        SET etl_result = CONCAT('Insert succeeded, row count = ', total_affected_rows);
        SET etl_status = 'succeeded';
    ELSE
        SET etl_result = CONCAT("ERROR ", error_sqlstate, " (", error_no, "): ", error_text);
        SET full_error = CONCAT("ERROR ", error_sqlstate, " (", error_no, "): ", error_text);
    END IF;
  
    -- insert etl log
    CALL prod_mcg_integration.insert_etl_log(
        etl_start_datetime, -- param_etl_start_timestamp
        NOW(), -- param_etl_finish_timestamp
        etl_status, -- param_etl_status
        'Stored Procedure', -- param_etl_done_by
        'prod_mcg_integration.replace_sales_heads()',-- param_script_name
        '-', -- param_type_of_data
        '-', -- param_source_system_name
        NULL, -- param_source_path
        'prod_mcg_staging', -- param_source_schema_name
        'sales_heads',
        '-', -- param_target_system_name
        NULL, -- param_target_path
        'prod_mcg_integration', -- param_target_schema_name
        'sales_heads',
        total_new_records - total_old_records, -- param_total_new_rows
        total_new_records, -- param_total_rows_in_target_system
        is_new_records_inserted, -- param_is_new_records_inserted
        'replace_sales_heads',
        full_error, -- param_error_log
        NULL -- param_remark
    );

-- *** C ***


end;
