-- prod_mcg_integration.branch_stock definition

CREATE TABLE `branch_stock` (
  `branch_id` varchar(50) NOT NULL,
  `product_id_sku` varchar(50) NOT NULL,
  `product_id_model` varchar(50) DEFAULT NULL,
  `lot_no` varchar(20) DEFAULT NULL,
  `qty` int(10) DEFAULT NULL,
  `current_datetime` datetime DEFAULT NULL COMMENT 'timestamp when updating the qty col',
  `in_transit_qty` int(10) DEFAULT NULL,
  `in_transit_datetime` datetime DEFAULT NULL,
  `on_delivery` int(10) DEFAULT NULL,
  `on_order` int(10) DEFAULT NULL,
  `latest_etl_datetime` datetime DEFAULT NULL,
  PRIMARY KEY (`branch_id`,`product_id_sku`),
  KEY `branch_stock_branch_id_IDX` (`branch_id`) USING BTREE,
  KEY `branch_stock_product_id_sku_IDX` (`product_id_sku`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;