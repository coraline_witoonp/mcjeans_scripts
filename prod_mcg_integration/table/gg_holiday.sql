-- prod_mcg_integration.gg_holiday definition

CREATE TABLE `gg_holiday` (
  `id` varchar(64) NOT NULL,
  `summary` varchar(128) NOT NULL,
  `start` date NOT NULL,
  `updated` datetime NOT NULL,
  PRIMARY KEY (`id`,`summary`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;