-- prod_mcg_integration.products definition

CREATE TABLE `products` (
  `product_id` varchar(30) NOT NULL,
  `product_core_id` varchar(30) DEFAULT NULL,
  `name` varchar(100) DEFAULT NULL,
  `is_active` tinyint(1) DEFAULT NULL,
  `is_focused` tinyint(1) DEFAULT NULL,
  `type` varchar(10) DEFAULT NULL,
  `attribute_group_id` varchar(20) DEFAULT NULL,
  `cost` decimal(10,2) DEFAULT NULL,
  `price` decimal(10,2) DEFAULT NULL,
  `category_level_1` varchar(100) DEFAULT NULL,
  `category_level_2` varchar(100) DEFAULT NULL,
  `category_level_3` varchar(100) DEFAULT NULL,
  `category_level_4` varchar(100) DEFAULT NULL,
  `category_level_5` varchar(100) DEFAULT NULL,
  `start_date` varchar(20) DEFAULT NULL,
  `end_date` varchar(20) DEFAULT NULL,
  `first_buy_date` date DEFAULT NULL,
  `last_buy_date` date DEFAULT NULL,
  `gender` char(1) DEFAULT NULL,
  `product_line` varchar(30) DEFAULT NULL,
  `sub_brand` varchar(30) DEFAULT NULL,
  `color` char(2) DEFAULT NULL,
  `size` char(2) DEFAULT NULL,
  `shape_1` varchar(30) DEFAULT NULL,
  `shape_2` varchar(30) DEFAULT NULL,
  `shape_3` varchar(30) DEFAULT NULL,
  `is_denim` tinyint(1) DEFAULT '0',
  `aging_color
aging_color_text` varchar(5) DEFAULT NULL,
  `aging_color_text` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`product_id`),
  KEY `product_core_id` (`product_core_id`),
  KEY `start_date` (`start_date`),
  KEY `is_focused` (`is_focused`),
  KEY `is_active` (`is_active`),
  KEY `focus` (`is_focused`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;