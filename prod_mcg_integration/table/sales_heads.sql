-- prod_mcg_integration.sales_heads definition

CREATE TABLE `sales_heads` (
  `sales_head_id` varchar(20) NOT NULL,
  `void_ref_id` varchar(20) DEFAULT NULL,
  `type` char(1) NOT NULL,
  `sold_date` date NOT NULL,
  `invoice_no` varchar(30) DEFAULT NULL,
  `total_qty` int(6) NOT NULL,
  `total_amount` decimal(16,2) DEFAULT NULL,
  `total_discount` decimal(16,2) DEFAULT NULL,
  `customer_id` varchar(30) DEFAULT NULL,
  `branch_id` varchar(6) NOT NULL,
  `branch_ref` varchar(40) DEFAULT NULL,
  `sales_channel` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`sales_head_id`),
  KEY `void_ref_id` (`void_ref_id`),
  KEY `type` (`type`),
  KEY `sold_date` (`sold_date`),
  KEY `invoice_no` (`invoice_no`),
  KEY `customer` (`customer_id`),
  KEY `branch_id` (`branch_id`),
  KEY `branch_ref` (`branch_ref`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;