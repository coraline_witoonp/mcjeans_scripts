-- prod_mcg_integration.etl_log definition

CREATE TABLE `etl_log` (
  `log_id` int(10) NOT NULL AUTO_INCREMENT,
  `etl_start_datetime` datetime DEFAULT NULL,
  `etl_finish_datetime` datetime DEFAULT NULL,
  `etl_status` varchar(10) DEFAULT NULL COMMENT 'only succeeded or fail',
  `etl_done_by` varchar(100) DEFAULT NULL COMMENT 'eg: Stored Procedure, AWS Lambda, AWS EC2, AWS Sage Maker',
  `script_name` varchar(255) DEFAULT NULL COMMENT 'eg: ri_integration.update_branch(), load_data_from_csv.py',
  `type_of_data` varchar(100) DEFAULT NULL COMMENT 'eg: master, txn',
  `source_system_name` varchar(100) DEFAULT NULL COMMENT 'eg: Amazon S3, MySQL, SQL-Server, CSV, TXT',
  `source_path` varchar(255) DEFAULT NULL COMMENT 'eg: s3://wacoal-data/qrms_transaction/qrms_transaction_20200313.csv',
  `source_schema_name` varchar(100) DEFAULT NULL COMMENT 'ri_integration, prod_wacoal_staging',
  `datasource_name` varchar(100) DEFAULT NULL COMMENT 'qrms_transaction_20200313.csv, QRMS_TRANSCTION',
  `target_system_name` varchar(100) DEFAULT NULL COMMENT 'eg: Amazon S3, MySQL, SQL-Server, CSV, TXT',
  `target_path` varchar(255) DEFAULT NULL COMMENT 'eg: s3://wacoal-data/qrms_transaction/qrms_transaction_20200314.csv',
  `target_schema_name` varchar(100) DEFAULT NULL COMMENT 'ri_integration, ri_feature, ri_result',
  `target_name` varchar(100) DEFAULT NULL COMMENT 'branch, product, qrms_transaction_20200314.csv',
  `total_new_rows` int(10) DEFAULT NULL,
  `total_rows_after_update` int(10) DEFAULT NULL,
  `is_new_records_inserted` tinyint(1) DEFAULT NULL,
  `etl_description` text,
  `error_log` text COMMENT 'error of MySQL, error of Python',
  `remark` text,
  PRIMARY KEY (`log_id`)
) ENGINE=InnoDB AUTO_INCREMENT=56 DEFAULT CHARSET=utf8;