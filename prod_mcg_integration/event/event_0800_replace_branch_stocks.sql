CREATE EVENT event_0800_replace_branch_stocks
ON SCHEDULE EVERY 1 DAY
STARTS '2020-03-20 01:00:00.000'
ON COMPLETION PRESERVE
ENABLE
COMMENT 'Update All Data 1 Time/Day on 08:00'
DO BEGIN

  Call prod_mcg_integration.replace_branch_stocks();

end;
