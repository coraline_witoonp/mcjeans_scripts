CREATE EVENT event_0700_replace_sales_lines
ON SCHEDULE EVERY 1 DAY
STARTS '2020-03-20 00:00:00.000'
ON COMPLETION PRESERVE
ENABLE
COMMENT 'Update All Data 1 Time/Day on 07:00'
DO BEGIN

  Call `prod_mcg_integration`.`replace_sales_lines`();

end;
