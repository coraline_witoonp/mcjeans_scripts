CREATE EVENT event_0745_replace_products
ON SCHEDULE EVERY 1 DAY
STARTS '2020-03-20 21:45:00.000'
ON COMPLETION PRESERVE
ENABLE
COMMENT 'Update All Data 1 Time/Day on 07:45'
DO BEGIN

  Call `prod_mcg_integration`.`replace_products`();

end;
