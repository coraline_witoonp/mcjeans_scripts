CREATE EVENT event_0600_replace_branches
ON SCHEDULE EVERY 1 DAY
STARTS '2020-03-20 23:00:00.000'
ON COMPLETION PRESERVE
ENABLE
COMMENT 'Update All Data 1 Time/Day on 06:00'
DO BEGIN

  Call prod_mcg_integration.replace_branches();

end;