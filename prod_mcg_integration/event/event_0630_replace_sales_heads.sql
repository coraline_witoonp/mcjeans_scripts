CREATE EVENT event_0630_replace_sales_heads
ON SCHEDULE EVERY 1 DAY
STARTS '2020-03-20 23:30:00.000'
ON COMPLETION PRESERVE
ENABLE
COMMENT 'Update All Data 1 Time/Day on 06:30'
DO BEGIN

  Call `prod_mcg_integration`.`replace_sales_heads`();

end;
