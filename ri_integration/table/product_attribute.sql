-- ri_integration.product_attribute definition

CREATE TABLE `product_attribute` (
  `product_att_id` int(10) NOT NULL AUTO_INCREMENT,
  `product_id_sku` varchar(50) DEFAULT NULL,
  `product_id_model` varchar(50) DEFAULT NULL,
  `product_att_set` varchar(50) DEFAULT NULL,
  `product_att_key` varchar(50) DEFAULT NULL,
  `product_att_value` varchar(255) DEFAULT NULL,
  `latest_etl_datetime` datetime DEFAULT NULL,
  PRIMARY KEY (`product_att_id`),
  UNIQUE KEY `product_attribute_product_att_key_all_idx` (`product_id_sku`,`product_id_model`,`product_att_set`,`product_att_key`),
  KEY `product_attribute_product_id_sku_idx` (`product_id_sku`) USING BTREE,
  KEY `product_attribute_product_id_model_idx` (`product_id_model`) USING BTREE,
  KEY `product_attribute_product_att_set_idx` (`product_att_set`) USING BTREE,
  KEY `product_attribute_product_att_key_idx` (`product_att_key`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=983026 DEFAULT CHARSET=utf8mb4;