-- ri_integration.product_color definition

CREATE TABLE `product_color` (
  `product_color_code` varchar(20) NOT NULL,
  `product_color_meaning_th` varchar(100) DEFAULT NULL,
  `product_color_meaning_en` varchar(100) DEFAULT NULL,
  `latest_etl_datetime` datetime DEFAULT NULL,
  PRIMARY KEY (`product_color_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;