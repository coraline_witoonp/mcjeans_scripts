-- ri_integration.product_size definition

CREATE TABLE `product_size` (
  `product_size_code` varchar(20) NOT NULL,
  `product_size_category` varchar(50) NOT NULL,
  `product_size_meaning` varchar(100) DEFAULT NULL,
  `latest_etl_datetime` datetime DEFAULT NULL,
  PRIMARY KEY (`product_size_category`,`product_size_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;