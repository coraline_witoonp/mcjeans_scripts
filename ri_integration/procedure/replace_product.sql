CREATE DEFINER=`admin`@`%` PROCEDURE `ri_integration`.`replace_product`()
begin

    
-- *** A ***
    -- Declare general variables
    DECLARE total_affected_rows INT(11) DEFAULT NULL;
    DECLARE total_rows_datasource INT(11) DEFAULT NULL;
    DECLARE query_result TEXT;
    DECLARE etl_start_datetime DATETIME;
    DECLARE etl_status VARCHAR(15) DEFAULT 'failed';
    DECLARE is_new_records_inserted TINYINT(1) DEFAULT NULL;
    DECLARE total_old_records INT(10) DEFAULT '0';
    DECLARE total_new_records INT(10) DEFAULT '0';
    DECLARE etl_result TEXT DEFAULT NULL;
    DECLARE full_error TEXT DEFAULT NULL;
    
    -- Declare variables for storing error
    DECLARE error_sqlstate VARCHAR(5) DEFAULT '00000';
    DECLARE error_no VARCHAR(30);
    DECLARE error_text TEXT;
    
    DECLARE CONTINUE HANDLER FOR SQLEXCEPTION
    BEGIN
        GET DIAGNOSTICS CONDITION 1
        error_sqlstate = RETURNED_SQLSTATE, 
        error_no = MYSQL_ERRNO, 
        error_text = MESSAGE_TEXT;
    END;
    
    -- Start executing query
    SET etl_start_datetime = NOW(); 
    
    -- Count total row from original source
    SELECT 
        COUNT(*) INTO total_rows_datasource 
    FROM prod_mcg_staging.sap_article;
-- *** A ***
-- *** B ***
    -- Count total row before inserting new records
    SELECT
        COUNT(*) INTO total_old_records
    FROM ri_integration.product;

-- RUN --
    
TRUNCATE ri_integration.product;


insert into ri_integration.product
select 
    material as product_id_sku,
    (if (length(material) >= 13,substring(material, 1, length(material)-4),null)) as product_id_model,
    article_description as product_name_th,
    null as product_name_en,
    article_size as product_size_code,
    article_color as product_color_code,
    brand as product_brand,
    sub_brand_text as product_sub_brand,
    mcl1text as product_category_1,
    mcl2text as product_category_2,
    mcl3text as product_category_3,
    mcl4text as product_category_4,
    mcl5text as product_category_5,
    concat(mcl3text , ':' ,mcl4text) as product_att_set,
    standard_cost_adj as product_cost_price,
    selling_price as product_selling_price,

    (case when concat(season_year,'0000')='0000' then null
          when substring(concat(season,'0000'),3,2)='00' then null
          else date(concat(season_year,'-',substring(season,3,2),'-01'))
    end) as product_create_date,

    null as product_launch_date, -- calculate by update
    
    if(mcl2text not in ('', 'anello', 'beauty buffet', 'mc space', 'naturetuch', 'other', 'other brands', 'rayban'), 1, 0) 
    as is_focused,
    
    null as is_active_in_one_year,  -- calculate by update
    null as min_datetime_in_sale_txn, -- calculate by update
    null as max_datetime_in_sale_txn,-- calculate by update
    null as product_picture_ref, -- new: link file
    now() as latest_etl_datetime,
    fashion_grade_desc as product_type
from
    prod_mcg_staging.sap_article;


-- UPDATE is_focused
/*update ri_integration.products
SET
    IS_FOCUSED = 1
WHERE
    CATEGORY_LEVEL_2 NOT IN ('', 'ANELLO', 'BEAUTY BUFFET', 'MC Space', 'NATURETUCH', 'OTHER', 'OTHER BRANDS', 'RAYBAN');*/


-- Special cases for a report but not for models
/*
update prod_mcg_integration.products
SET
    IS_FOCUSED = 9
WHERE
    PRODUCT_ID IN ('000000006000000001', '000000006000000002');
*/


-- RUN --

    -- Count total row after inserting new records
    SELECT
        COUNT(*) INTO total_new_records
    FROM ri_integration.product;
    
-- *** B ***
-- *** C ***

    IF total_new_records - total_old_records > 0 THEN
        SET is_new_records_inserted = 0;
    ELSE
        SET is_new_records_inserted = 1;
    END IF;

    IF total_new_records - total_old_records > 0 THEN
        SET is_new_records_inserted = 1;
    ELSE
        SET is_new_records_inserted = 0;
    END IF;

    
    -- Check whether the insert was successful
    IF error_sqlstate = '00000' THEN
        GET DIAGNOSTICS total_affected_rows = ROW_COUNT;
        SET etl_result = CONCAT('Insert succeeded, row count = ', total_affected_rows);
        SET etl_status = 'succeeded';
    ELSE
        SET etl_result = CONCAT("ERROR ", error_sqlstate, " (", error_no, "): ", error_text);
        SET full_error = CONCAT("ERROR ", error_sqlstate, " (", error_no, "): ", error_text);
    END IF;
  
    -- insert etl log
    CALL ri_integration.insert_etl_log(
        etl_start_datetime, -- param_etl_start_timestamp
        NOW(), -- param_etl_finish_timestamp
        etl_status, -- param_etl_status
        'Stored Procedure', -- param_etl_done_by
        'ri_integration.replace_product()',-- param_script_name
        '-', -- param_type_of_data
        '-', -- param_source_system_name
        NULL, -- param_source_path
        'prod_mcg_staging', -- param_source_schema_name
        'sap_article',
        '-', -- param_target_system_name
        NULL, -- param_target_path
        'ri_integration', -- param_target_schema_name
        'product',
        total_new_records - total_old_records, -- param_total_new_rows
        total_new_records, -- param_total_rows_in_target_system
        is_new_records_inserted, -- param_is_new_records_inserted
        'replace_product',
        full_error, -- param_error_log
        NULL -- param_remark
    );

-- *** C ***


end;
