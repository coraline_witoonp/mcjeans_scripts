CREATE DEFINER=`admin`@`%` PROCEDURE `ri_integration`.`replace_product_attribute`()
begin

    
-- *** A ***
    -- Declare general variables
    DECLARE total_affected_rows INT(11) DEFAULT NULL;
    DECLARE total_rows_datasource INT(11) DEFAULT NULL;
    DECLARE query_result TEXT;
    DECLARE etl_start_datetime DATETIME;
    DECLARE etl_status VARCHAR(15) DEFAULT 'failed';
    DECLARE is_new_records_inserted TINYINT(1) DEFAULT NULL;
    DECLARE total_old_records INT(10) DEFAULT '0';
    DECLARE total_new_records INT(10) DEFAULT '0';
    DECLARE etl_result TEXT DEFAULT NULL;
    DECLARE full_error TEXT DEFAULT NULL;
    
    -- Declare variables for storing error
    DECLARE error_sqlstate VARCHAR(5) DEFAULT '00000';
    DECLARE error_no VARCHAR(30);
    DECLARE error_text TEXT;
    
    DECLARE CONTINUE HANDLER FOR SQLEXCEPTION
    BEGIN
        GET DIAGNOSTICS CONDITION 1
        error_sqlstate = RETURNED_SQLSTATE, 
        error_no = MYSQL_ERRNO, 
        error_text = MESSAGE_TEXT;
    END;
    
    -- Start executing query
    SET etl_start_datetime = NOW(); 
    
    -- Count total row from original source
    SELECT 
        COUNT(*) INTO total_rows_datasource 
    FROM prod_mcg_staging.sap_article;
-- *** A ***
-- *** B ***
    -- Count total row before inserting new records
    SELECT
        COUNT(*) INTO total_old_records
    FROM ri_integration.product_attribute;

-- RUN --

TRUNCATE ri_integration.product_attribute;
-- ===
-- 1.season
insert into ri_integration.product_attribute
(
    -- product_att_id,
    product_id_sku,
    product_id_model,
    product_att_set,
    product_att_key,
    product_att_value,
    latest_etl_datetime
)
select
    -- auto_increment as product_att_id,
    material as product_id_sku,
    (if (length(material) >= 13,substring(material, 1, length(material)-4),null)) as product_id_model,
    concat(mcl3text , ':' ,mcl4text) as product_att_set,
    'launch_season' as product_att_key,
    season as product_att_value,
    now() as latest_etl_datetime
from
    prod_mcg_staging.sap_article
where 
    (season) is not null;

-- ===
-- 2.product_line
insert into ri_integration.product_attribute
(
    -- product_att_id,
    product_id_sku,
    product_id_model,
    product_att_set,
    product_att_key,
    product_att_value,
    latest_etl_datetime
)
select
    -- auto_increment as product_att_id,
    material as product_id_sku,
    (if (length(material) >= 13,substring(material, 1, length(material)-4),null)) as product_id_model,
    concat(mcl3text , ':' ,mcl4text) as product_att_set,
    'product_line' as product_att_key,
    product_line as product_att_value,
    now() as latest_etl_datetime
from
    prod_mcg_staging.sap_article
where 
    (product_line) is not null;

-- ===
-- 3.product_line_text
insert into ri_integration.product_attribute
(
    -- product_att_id,
    product_id_sku,
    product_id_model,
    product_att_set,
    product_att_key,
    product_att_value,
    latest_etl_datetime
)
select
    -- auto_increment as product_att_id,
    material as product_id_sku,
    (if (length(material) >= 13,substring(material, 1, length(material)-4),null)) as product_id_model,
    concat(mcl3text , ':' ,mcl4text) as product_att_set,
    'product_line_text' as product_att_key,
    product_line_text as product_att_value,
    now() as latest_etl_datetime
from
    prod_mcg_staging.sap_article
where 
    (product_line_text) is not null;  

-- ===
-- 4.gender

insert into ri_integration.product_attribute
(
    -- product_att_id,
    product_id_sku,
    product_id_model,
    product_att_set,
    product_att_key,
    product_att_value,
    latest_etl_datetime
)
select
    -- auto_increment as product_att_id,
    material as product_id_sku,
    (if (length(material) >= 13,substring(material, 1, length(material)-4),null)) as product_id_model,
    concat(mcl3text , ':' ,mcl4text) as product_att_set,
    'gender' as product_att_key,
    gender as product_att_value,
    now() as latest_etl_datetime
from
    prod_mcg_staging.sap_article
where 
    (gender) is not null;

-- ===
-- 5.gender_text

insert into ri_integration.product_attribute
(
    -- product_att_id,
    product_id_sku,
    product_id_model,
    product_att_set,
    product_att_key,
    product_att_value,
    latest_etl_datetime
)
select
    -- auto_increment as product_att_id,
    material as product_id_sku,
    (if (length(material) >= 13,substring(material, 1, length(material)-4),null)) as product_id_model,
    concat(mcl3text , ':' ,mcl4text) as product_att_set,
    'gender_text' as product_att_key,
    gender_text as product_att_value,
    now() as latest_etl_datetime
from
    prod_mcg_staging.sap_article
where 
    (gender_text) is not null;


-- ===
-- 6.aging_color

insert into ri_integration.product_attribute
(
    -- product_att_id,
    product_id_sku,
    product_id_model,
    product_att_set,
    product_att_key,
    product_att_value,
    latest_etl_datetime
)
select
    -- auto_increment as product_att_id,
    material as product_id_sku,
    (if (length(material) >= 13,substring(material, 1, length(material)-4),null)) as product_id_model,
    concat(mcl3text , ':' ,mcl4text) as product_att_set,
    'aging_color' as product_att_key,
    aging_color as product_att_value,
    now() as latest_etl_datetime
from
    prod_mcg_staging.sap_article
where 
    (aging_color) is not null;


-- ===
-- 7.aging_color_text

insert into ri_integration.product_attribute
(
    -- product_att_id,
    product_id_sku,
    product_id_model,
    product_att_set,
    product_att_key,
    product_att_value,
    latest_etl_datetime
)
select
    -- auto_increment as product_att_id,
    material as product_id_sku,
    (if (length(material) >= 13,substring(material, 1, length(material)-4),null)) as product_id_model,
    concat(mcl3text , ':' ,mcl4text) as product_att_set,
    'aging_color_text' as product_att_key,
    aging_color_text as product_att_value,
    now() as latest_etl_datetime
from
    prod_mcg_staging.sap_article
where 
    (aging_color_text) is not null;

-- ===
-- 8.prd_attr_x

insert into ri_integration.product_attribute
(
    -- product_att_id,
    product_id_sku,
    product_id_model,
    product_att_set,
    product_att_key,
    product_att_value,
    latest_etl_datetime
)
select
    -- auto_increment as product_att_id,
    material as product_id_sku,
    (if (length(material) >= 13,substring(material, 1, length(material)-4),null)) as product_id_model,
    concat(mcl3text , ':' ,mcl4text) as product_att_set,
    'prd_attr_x' as product_att_key,
    prd_attr_x as product_att_value,
    now() as latest_etl_datetime
from
    prod_mcg_staging.sap_article
where 
    (prd_attr_x) is not null;

-- ===
-- 9.prd_attr_x_text

insert into ri_integration.product_attribute
(
    -- product_att_id,
    product_id_sku,
    product_id_model,
    product_att_set,
    product_att_key,
    product_att_value,
    latest_etl_datetime
)
select
    -- auto_increment as product_att_id,
    material as product_id_sku,
    (if (length(material) >= 13,substring(material, 1, length(material)-4),null)) as product_id_model,
    concat(mcl3text , ':' ,mcl4text) as product_att_set,
    'prd_attr_x_text' as product_att_key,
    prd_attr_x_text as product_att_value,
    now() as latest_etl_datetime
from
    prod_mcg_staging.sap_article
where 
    (prd_attr_x_text) is not null;

-- ===
-- 10.shape_1

insert into ri_integration.product_attribute
(
    -- product_att_id,
    product_id_sku,
    product_id_model,
    product_att_set,
    product_att_key,
    product_att_value,
    latest_etl_datetime
)
select
    -- auto_increment as product_att_id,
    material as product_id_sku,
    (if (length(material) >= 13,substring(material, 1, length(material)-4),null)) as product_id_model,
    concat(mcl3text , ':' ,mcl4text) as product_att_set,
    'shape_1' as product_att_key,
    shape_1 as product_att_value,
    now() as latest_etl_datetime
from
    prod_mcg_staging.sap_article
where 
    (shape_1) is not null;

-- ===
-- 11.shape_1_text

insert into ri_integration.product_attribute
(
    -- product_att_id,
    product_id_sku,
    product_id_model,
    product_att_set,
    product_att_key,
    product_att_value,
    latest_etl_datetime
)
select
    -- auto_increment as product_att_id,
    material as product_id_sku,
    (if (length(material) >= 13,substring(material, 1, length(material)-4),null)) as product_id_model,
    concat(mcl3text , ':' ,mcl4text) as product_att_set,
    'shape_1_text' as product_att_key,
    shape_1_text as product_att_value,
    now() as latest_etl_datetime
from
    prod_mcg_staging.sap_article
where 
    (shape_1_text) is not null;

-- ===
-- 12.shape_2

insert into ri_integration.product_attribute
(
    -- product_att_id,
    product_id_sku,
    product_id_model,
    product_att_set,
    product_att_key,
    product_att_value,
    latest_etl_datetime
)
select
    -- auto_increment as product_att_id,
    material as product_id_sku,
    (if (length(material) >= 13,substring(material, 1, length(material)-4),null)) as product_id_model,
    concat(mcl3text , ':' ,mcl4text) as product_att_set,
    'shape_2' as product_att_key,
    shape_2 as product_att_value,
    now() as latest_etl_datetime
from
    prod_mcg_staging.sap_article
where 
    (shape_2) is not null;

-- ===
-- 13.shape_2_text

insert into ri_integration.product_attribute
(
    -- product_att_id,
    product_id_sku,
    product_id_model,
    product_att_set,
    product_att_key,
    product_att_value,
    latest_etl_datetime
)
select
    -- auto_increment as product_att_id,
    material as product_id_sku,
    (if (length(material) >= 13,substring(material, 1, length(material)-4),null)) as product_id_model,
    concat(mcl3text , ':' ,mcl4text) as product_att_set,
    'shape_2_text' as product_att_key,
    shape_2_text as product_att_value,
    now() as latest_etl_datetime
from
    prod_mcg_staging.sap_article
where 
    (shape_2_text) is not null;

-- ===
-- 14.shape_3

insert into ri_integration.product_attribute
(
    -- product_att_id,
    product_id_sku,
    product_id_model,
    product_att_set,
    product_att_key,
    product_att_value,
    latest_etl_datetime
)
select
    -- auto_increment as product_att_id,
    material as product_id_sku,
    (if (length(material) >= 13,substring(material, 1, length(material)-4),null)) as product_id_model,
    concat(mcl3text , ':' ,mcl4text) as product_att_set,
    'shape_3' as product_att_key,
    shape_3 as product_att_value,
    now() as latest_etl_datetime
from
    prod_mcg_staging.sap_article
where 
    (shape_3) is not null;

-- ===
-- 15.shape_3_text
insert into ri_integration.product_attribute
(
    -- product_att_id,
    product_id_sku,
    product_id_model,
    product_att_set,
    product_att_key,
    product_att_value,
    latest_etl_datetime
)
select
    -- auto_increment as product_att_id,
    material as product_id_sku,
    (if (length(material) >= 13,substring(material, 1, length(material)-4),null)) as product_id_model,
    concat(mcl3text , ':' ,mcl4text) as product_att_set,
    'shape_3_text' as product_att_key,
    shape_3_text as product_att_value,
    now() as latest_etl_datetime
from
    prod_mcg_staging.sap_article
where 
    (shape_3_text) is not null;

-- ====

-- RUN --

    -- Count total row after inserting new records
    SELECT
        COUNT(*) INTO total_new_records
    FROM ri_integration.product_attribute;
    
-- *** B ***
-- *** C ***

    IF total_new_records - total_old_records > 0 THEN
        SET is_new_records_inserted = 0;
    ELSE
        SET is_new_records_inserted = 1;
    END IF;

    IF total_new_records - total_old_records > 0 THEN
        SET is_new_records_inserted = 1;
    ELSE
        SET is_new_records_inserted = 0;
    END IF;

    
    -- Check whether the insert was successful
    IF error_sqlstate = '00000' THEN
        GET DIAGNOSTICS total_affected_rows = ROW_COUNT;
        SET etl_result = CONCAT('Insert succeeded, row count = ', total_affected_rows);
        SET etl_status = 'succeeded';
    ELSE
        SET etl_result = CONCAT("ERROR ", error_sqlstate, " (", error_no, "): ", error_text);
        SET full_error = CONCAT("ERROR ", error_sqlstate, " (", error_no, "): ", error_text);
    END IF;
  
    -- insert etl log
    CALL ri_integration.insert_etl_log(
        etl_start_datetime, -- param_etl_start_timestamp
        NOW(), -- param_etl_finish_timestamp
        etl_status, -- param_etl_status
        'Stored Procedure', -- param_etl_done_by
        'ri_integration.replace_product_attribute()',-- param_script_name
        '-', -- param_type_of_data
        '-', -- param_source_system_name
        NULL, -- param_source_path
        'prod_mcg_staging', -- param_source_schema_name
        'sap_article',
        '-', -- param_target_system_name
        NULL, -- param_target_path
        'ri_integration', -- param_target_schema_name
        'product_attribute',
        total_new_records - total_old_records, -- param_total_new_rows
        total_new_records, -- param_total_rows_in_target_system
        is_new_records_inserted, -- param_is_new_records_inserted
        'replace_product_attribute',
        full_error, -- param_error_log
        NULL -- param_remark
    );

-- *** C ***


end;
