CREATE EVENT event_0930_replace_product_attribute
ON SCHEDULE EVERY 1 DAY
STARTS '2020-03-20 02:30:00.000'
ON COMPLETION PRESERVE
ENABLE
COMMENT 'Update All Data 1 Time/Day on 09:30'
DO BEGIN

  Call ri_integration.replace_product_attribute();

end;
