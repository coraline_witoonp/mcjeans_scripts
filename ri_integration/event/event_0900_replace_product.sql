CREATE EVENT event_0900_replace_product
ON SCHEDULE EVERY 1 DAY
STARTS '2020-03-20 02:00:00.000'
ON COMPLETION PRESERVE
ENABLE
COMMENT 'Update All Data 1 Time/Day on 09:00'
DO BEGIN

  Call ri_integration.replace_product();

end;
